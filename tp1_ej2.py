# Tp1 Ejer 2

import math as m



def e2n1(x):				# Toma x y devuelve log(x+1,2) si el output es entero o 0 si no lo es
	res_e2n1=0
	n=m.log(x+1,2)
	if n%1 == 0:
		res_e2n1= n
	return res_e2n1 



def esprimo(x):				# Toma un numero y devuelve 1 si es primo o 0 si no lo es
	c=0
	res_primo=0
	for i in range(1,x+1):
		if x%i==0:
			c=c+1
	if c==2:
		res_primo= 1
	return res_primo




def orion(n):						# Funcion principal, usa las dos funciones anteriores.
	res=1
	suma=0
	while suma != n:
		A=e2n1(res)*esprimo(res)
		if A != 0:
			A=1
		suma=suma+A
		res=res+1
		#print(res)
	return res-1					

inp=3								# Input a ingresar

print(orion(inp))




